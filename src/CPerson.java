public class CPerson {
    private String name;
    private String address;
    
    public CPerson(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public CPerson(CPerson person) {
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "CPerson [name=" + name + ", address=" + address + "]";
    } 
}
