public class App {
    public static void main(String[] args) throws Exception {
        CPerson person1 = new CPerson("tuan", "HCM");
        CPerson person2 = new CPerson("duong", "HN");

        System.out.println("--- subtask5: tao 2 doi tuong person ---");
        System.out.println(person1);
        System.out.println(person2);

        System.out.println("--- subtask6: tao 2 doi tuong student---");
        CStudent student1 = new CStudent("phu", "DN", "hoa", 2022, 15000);
        CStudent student2 = new CStudent("phu2", "DN2", "hoa2", 202, 15000);
        System.out.println(student1);
        System.out.println(student2);

        System.out.println("--- subtask7: tao 2 doi tuong staff---");
        CStaff staff1 = new CStaff("duy", "HCM", "devcamp", 20000);
        CStaff staff2 = new CStaff("phu", "HN", "devcamp2", 30000);
        System.out.println(staff1);
        System.out.println(staff2);
    }
}
